import * as types from './types'

export const setNetworkStatus = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: types.SET_NETWORK_STATUS,
      data,
      status: data === 'none' ? true : false
    })
  }
}
