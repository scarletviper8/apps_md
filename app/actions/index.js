import * as merchantActions from './merchant'
import * as userActions from './user'
import * as homeActions from './home'
import * as broadcastActions from './broadcast'
import * as commonActions from './common'
import * as notificationAction from './notification'

export const ActionCreators = Object.assign({},
  merchantActions,
  userActions,
  homeActions,
  broadcastActions,
  commonActions,
  notificationAction
)
