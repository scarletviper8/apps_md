import * as types from './types'
import BillboardApi from '../api/home'
import _ from 'lodash'
const uuidv1 = require('uuid/v1')


export const billboard = () => (dispatch, getState) => {
  let data = {
    token: null
  }
  BillboardApi.billboard(data, (res) => {
    if (res.status === 200) {
      const billboardData = _.map(res.data, o => _.extend({uid: uuidv1()}, o));

      dispatch({
        type: types.BILLBOARD,
        payload: billboardData,
      })
    } else {
      dispatch({
        type: types.BILLBOARD,
        payload: [],
      })
    }
  })
}
