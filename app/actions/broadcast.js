import * as types from './types'
import BroadcastApi from '../api/broadcast'
import { AsyncStorage } from 'react-native'
import _ from 'lodash'
const uuidv1 = require('uuid/v1')


export const broadcastList = (offset) => (dispatch, getState) => {
  let data = {
    memberId: getState().userReducer.loginInfo.memberId,
    offset,
    token: null
  }
  BroadcastApi.broadcastList(data, (res) => {
    if(res.status === 200){
      const broadcastData = _.map(res.data, o => _.extend({uid: uuidv1()}, o));
      dispatch({
        type: types.BROADCAST_READ,
        payload: null,
      })
      dispatch({
        type: types.BROADCAST_LIST,
        payload: Object.assign([],broadcastData),
      })
    }

  })
}

export const broadcastDetail = () => (dispatch, getState) => {
  let messageId = getState().broadcastReducer.selectBroadcastId
  let data = {
    broadcastId: messageId,
    token: null
  }
  BroadcastApi.broadcastDetail(data, (res) => {
    if (res.data.length > 0) {
      const broadcastList = getState().broadcastReducer.broadcastListData
      let index = _.findIndex(broadcastList, ['broadcastId', res.data[0].broadcastId])
      broadcastList[index].isread = '1'
      dispatch({
        type: types.BROADCAST_LIST,
        payload: broadcastList,
      })
    }
    dispatch({
      type: types.BROADCAST_DETAIL,
      payload: res.data,
    })
  })
}

export const readBroadcast = () => (dispatch, getState) => {
  let data = {
    broadcastId: getState().broadcastReducer.selectBroadcastId,
    memberId: getState().userReducer.loginInfo.memberId,
    token: null
  }
  BroadcastApi.readBroadcast(data, (res) => {
    dispatch({
      type: types.BROADCAST_READ,
      payload: res.data,
    })
  })
}

export const setSelectedId = (broadcastId) => (dispatch, getState) => {

  dispatch({
    type: types.BROADCAST_SELECT,
    payload:broadcastId,
  })
}
export const resetBroadcastData = () => (dispatch, getState) => {
  dispatch({
    type: types.BROADCAST_RESET,
  })
}
