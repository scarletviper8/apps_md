import * as types from './types'
import UserApi from '../api/user'
import { AsyncStorage } from 'react-native'
import { NavigationActions } from 'react-navigation'
import NavigationService from '../navigators/NavigationService'
export const checkNumber = (number) => (dispatch, getState) => {
  let data = {
    number,
    token: null
  }

  UserApi.checkNumber(data, (res) => {
    let route,resetAction;
    if (res.status === 200 && res.data != 0) {
      const userReducer = getState().userReducer
      let loginInfo = userReducer.loginInfo ? userReducer.loginInfo : {}
      loginInfo.memberId = res.data.memberId
      loginInfo.isNewUser = false
      loginInfo.verified = true
      AsyncStorage.setItem('Kuma_loginInfo', JSON.stringify(loginInfo))
      route = 'Home'
      resetAction = NavigationActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName: route })],
      });

      dispatch({
        type: types.SET_NUMBER_INFO,
        payload: loginInfo,
        error: null
      })
    } else if (res.status === 200 && res.data === 0) {
      route = 'Register'
      resetAction = NavigationActions.navigate({
        routeName:route
      });
    } else {
      dispatch({
        type: types.CHECK_ERROR,
        payload: null,
        error: res.data
      })
    }
    dispatch(resetAction);
  })
}

export const setLoginInfo = (data) => (dispatch) => {
  AsyncStorage.setItem('Kuma_loginInfo', JSON.stringify(data))
  dispatch({
    type: types.SET_NUMBER_INFO,
    payload: data
  })
}

export const createMember = (bodyValues) => (dispatch, getState) => {
  let data = {
    bodyValues:bodyValues,
    token: null
  }

  let loginInfo = getState().userReducer.loginInfo
  dispatch({
    type: types.SET_NUMBER_INFO,
    payload: loginInfo,
    error: null
  })
  UserApi.checkEmail({ token: null, email: bodyValues.email }, (emailRes) => {
    if (emailRes.status === 200 && emailRes.data === 0) {
      UserApi.createMember(data, (res) => {
        if (res.status  === 201) {
          loginInfo.memberId = res.data.memberId
          loginInfo.isNewUser = true
          AsyncStorage.setItem('Kuma_loginInfo', JSON.stringify(loginInfo))
          const resetAction = NavigationActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: 'Home' })],
          });
          dispatch({
            type: types.SET_NUMBER_INFO,
            payload: loginInfo,
            error: null
          })
          dispatch(resetAction)
        } else {
          dispatch({
            type: types.CREATE_MEMBER_ERROR,
            error: res.data
          })
        }
      })
    } else {
      dispatch({
        type: types.CREATE_MEMBER_ERROR,
        error: { message: 'Email is already in use' }
      })
    }
  })

}

export const getSingleMember = () => (dispatch, getState) => {
  let data = {
    id: getState().userReducer.loginInfo.memberId,
    token: null
  }

  UserApi.getSingleMember(data, (res) => {
    if (res.status  === 200) {
      dispatch({
        type: types.GET_SINGLE_MEMBER,
        payload: res.data,
        error: null
      })
    } else {
      dispatch({
        type: types.GET_SINGLE_MEMBER,
        payload: null,
        error: res.data
      })
    }
  })
}

export const checkEmail = (email) => (dispatch, getState) => {
  const userReducer = getState().userReducer
  let loginInfo = userReducer.loginInfo ? userReducer.loginInfo : {}
  let data = {
    email: loginInfo.profile.email,
    token: null
  }

  UserApi.checkEmail(data, (res) => {
    let route,resetAction;
    if (res.status === 200 && res.data != 0) {
      loginInfo.email = res.data.email
      loginInfo.memberId = res.data.memberId
      loginInfo.isNewUser = false
      loginInfo.verified = true
      AsyncStorage.setItem('Kuma_loginInfo', JSON.stringify(loginInfo))
      route = 'Home'
      resetAction = NavigationActions.reset({
        index: 0,
        key: null,
        actions: [NavigationActions.navigate({ routeName: route })],
      });
      dispatch({
        type: types.SET_NUMBER_INFO,
        payload: loginInfo,
        error: null
      })
    } else if (res.status === 200 && res.data === 0) {
      route = 'Register'
      resetAction = NavigationActions.navigate({
        routeName:route
      });
    } else {
      dispatch({
        type: types.CHECK_ERROR,
        payload: null,
        error: res.data
      })
    }
    if (route) {
      dispatch(resetAction);
    }
  })
}

export const setVerificationFailureStatus = () => {
  return (dispatch, getState) => {
    const count = getState().userReducer.verificationFailureCount + 1
    dispatch({
      type: types.VERIFICATION_FAILURE,
      payload: count
    })
  }
}

export const resetVerficationFailureCount = () => {
  return (dispatch) => {
    dispatch({ type: 'RESET_VERIFICATION_FAILURE' })
  }
}

export const setPlayerId = (id) => {
  return (dispatch) => {
    dispatch({
      type: types.SET_PLAYER_ID,
      payload: id
    })
  }
}

export const updatePlayerId = () => {
  return (dispatch, getState) => {
    const data = {
      token: null,
      memberId: getState().userReducer.loginInfo.memberId,
      playerId: getState().userReducer.playerId
    }

    UserApi.updatePlayerId(data, (res) => {
      dispatch({
        type: types.UPDATE_PLAYER_ID,
        payload: res
      })
    })
  }
}

export const deletePlayerId = () => {
  return (dispatch, getState) => {
    const data = {
      token: null,
      playerId: getState().userReducer.playerId
    }

    UserApi.deletePlayerId(data, (res) => {
      dispatch({
        type: types.DELETE_PLAYER_ID,
        payload: res
      })
    })
  }
}

export const tesdata = (data) => (dispatch) => {
  // route = 'Leadview'
  // resetAction = NavigationActions.navigate({
  //   routeName:route
  // });
  // dispatch(resetAction)
  NavigationService.navigate('Leadview', null);
}
