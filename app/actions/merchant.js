import * as types from './types'
import _ from 'lodash'
import MerchantApi from '../api/merchant'

const uuidv1 = require('uuid/v1');

export const toggleMerchantFilterModal = (value) => (dispatch, getState) => {
  let data = getState().merchantReducer.filterModalOpen
  dispatch({
    type: types.TOGGLE_MERCHANT_FILTER_MODAL,
    payload: !data
  })
}
export const merchantList = () => (dispatch, getState) => {
    let data = {
      token: null
    }
  MerchantApi.merchantList(data, (res) => {
      dispatch({
        type: types.MERCHANT_LIST,
        payload: res.data,
      })
  })
}
export const memberMerchant = () => (dispatch, getState) => {
    let data = {
      id : getState().userReducer.loginInfo.memberId,
      token: null
    }
  MerchantApi.memberMerchant(data, (res) => {
      dispatch({
        type: types.MEMBER_MERCHANT,
        payload: res.data,
      })
  })
}

export const merchantCity = () => (dispatch, getState) => {
    let data = {
      token: null
    }
  MerchantApi.merchantCity(data, (res) => {
      dispatch({
        type: types.MERCHANT_CITY,
        payload: res.data,
      })
  })
}

export const merchantBanner = () => (dispatch, getState) => {
    let data = {
      userId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.merchantBanner(data, (res) => {
      dispatch({
        type: types.MERCHANT_BANNER,
        payload: res.data,
      })
  })
}

export const getMerchantPoints = () => (dispatch, getState) => {
    let data = {
      id: getState().userReducer.loginInfo.memberId,
      merchantsId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.points(data, (res) => {
      dispatch({
        type: types.MERCHANT_POINTS,
        payload: res.data,
      })
  })
}

export const detailDataMerchant = () => (dispatch, getState) => {
    let data = {
      userId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.detailDataMerchant(data, (res) => {
      dispatch({
        type: types.MERCHANT_DETAIL,
        payload: res.data,
      })
  })
}

export const getMerchantReward = () => (dispatch, getState) => {
    let data = {
      merchantId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.merchantReward(data, (res) => {
      dispatch({
        type: types.MERCHANT_REWARD,
        payload: res.data,
      })
  })
}

export const getMerchantListItem = () => (dispatch, getState) => {
    let data = {
      merchantId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.merchantListItem(data, (res) => {
      dispatch({
        type: types.MERCHANT_LIST_ITEM,
        payload: res.data,
      })
  })
}

export const getMerchantLocation = () => (dispatch, getState) => {
    let data = {
      merchantId: getState().merchantReducer.merchantSelectData,
      token: null
    }
  MerchantApi.merchantLocation(data, (res) => {
      dispatch({
        type: types.MERCHANT_LOCATION,
        payload: res.data,
      })
  })
}

export const getMerchantListData = (sort,city,offset) => (dispatch, getState) => {
    let data = {
      sort,
      city,
      offset,
      token: null
    }
  MerchantApi.merchantListData(data, (res) => {
    const data = _.map(res.data, o => _.extend({uid: uuidv1()}, o));
      dispatch({
        type: types.MERCHANT_LIST_DATA,
        payload: {
          data: data,
          offset
        }
      })
  })
}

export const setSelectedUserId = (userId) => (dispatch, getState) => {

      dispatch({
        type: types.MERCHANT_SELECT,
        payload:userId,
      })
}

export const resetMerchantData = () => (dispatch, getState) => {
  dispatch({
    type: types.MERCHANT_RESET,
  })
}
