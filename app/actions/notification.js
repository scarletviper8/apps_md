import * as types from './types'

export const setNotification = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: types.SHOW_NEW_NOTIFICATION_ICON,
      data
    })
  }
}
