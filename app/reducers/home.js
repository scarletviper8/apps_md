import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  billboardData: null,
}

export const homeReducer = createReducer(initialstate,{
  [types.BILLBOARD](state,action) {
    return Object.assign({}, state, {
      billboardData: action.payload,
    })
  }
})
