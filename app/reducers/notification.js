import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  showNewNotificationIcon: false,
}

export const notificationReducer = createReducer(initialstate,{
  [types.SHOW_NEW_NOTIFICATION_ICON](state,action) {
    return Object.assign({}, state, {
      showNewNotificationIcon: action.data,
    })
  }
})
