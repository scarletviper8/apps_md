import { combineReducers } from 'redux';

import * as navReducer from './navigation'
import * as merchantReducer from './merchant'
import * as userReducer from './user'
import * as homeReducer from './home'
import * as broadcastReducer from './broadcast'
import * as commonReducer from './common'
import * as notificationReducer from './notification'

export default combineReducers(Object.assign(
  navReducer,
  merchantReducer,
  userReducer,
  homeReducer,
  broadcastReducer,
  commonReducer,
  notificationReducer
));
