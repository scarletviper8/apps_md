import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  broadcastListData: null,
  broadcastDetailData: null,
  readBroadcastData: null,
  selectBroadcastId: null
}

export const broadcastReducer = createReducer(initialstate,{
  [types.BROADCAST_LIST](state,action) {
    return Object.assign({}, state, {
      broadcastListData: action.payload,
    })
  },
  [types.BROADCAST_DETAIL](state,action) {
    return Object.assign({}, state, {
      broadcastDetailData: action.payload,
    })
  },
  [types.BROADCAST_READ](state,action) {
    return Object.assign({}, state, {
      readBroadcastData: action.payload,
    })
  },
  [types.BROADCAST_SELECT](state,action){
    return Object.assign({}, state, {
      selectBroadcastId: action.payload,
    })
  },
  [types.BROADCAST_RESET](state,action){
    return Object.assign({}, state, {
      broadcastDetailData: null
    })
  }
})
