import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  filterModalOpen: false,
  merchantData: null,
  memberMerchantData: null,
  merchantCityData: null,
  merchantBannerData: null,
  merchantPoints: null,
  merchantDetail: null,
  merchantReward: null,
  merchantListItem: null,
  merchantLocation: null,
  merchantListData: [],
  merchantSelectData: null,
}

export const merchantReducer = createReducer(initialstate,{
  // toggle filter modal of merchant list
  [types.TOGGLE_MERCHANT_FILTER_MODAL](state,action) {
    return Object.assign({}, state, {
      filterModalOpen: action.payload
    })
  },
  [types.MERCHANT_LIST](state,action) {
    return Object.assign({}, state, {
      merchantData: action.payload,
    })
  },
  [types.MEMBER_MERCHANT](state,action) {
    return Object.assign({}, state, {
      memberMerchantData: action.payload,
    })
  },
  [types.MERCHANT_CITY](state,action) {
    return Object.assign({}, state, {
      merchantCityData: action.payload,
    })
  },
  [types.MERCHANT_BANNER](state,action) {
    return Object.assign({}, state, {
      merchantBannerData: action.payload,
    })
  },
  [types.MERCHANT_POINTS](state,action) {
    return Object.assign({}, state, {
      merchantPoints: action.payload,
    })
  },
  [types.MERCHANT_DETAIL](state,action) {
    return Object.assign({}, state, {
      merchantDetail: action.payload,
    })
  },
  [types.MERCHANT_REWARD](state,action) {
    return Object.assign({}, state, {
      merchantReward: action.payload,
    })
  },
  [types.MERCHANT_LIST_ITEM](state,action) {
    return Object.assign({}, state, {
      merchantListItem: action.payload,
    })
  },
  [types.MERCHANT_LOCATION](state,action) {
    return Object.assign({}, state, {
      merchantLocation: action.payload,
    })
  },
  [types.MERCHANT_LIST_DATA](state,action) {
    return Object.assign({}, state, {
      merchantListData: (action.payload.offset === 0 ? action.payload.data :  [...state.merchantListData, ...action.payload.data]),
    })
  },
  [types.MERCHANT_SELECT](state,action) {
    return Object.assign({}, state, {
      merchantSelectData: action.payload,
    })
  },
  [types.MERCHANT_RESET](state,action) {
    return Object.assign({}, state, {
      merchantPoints: null,
      merchantDetail: null,
      merchantReward: null,
      merchantListItem: null,
      merchantLocation: null,
      merchantBannerData: null,
      merchantSelectData: null
    })
  }
})
