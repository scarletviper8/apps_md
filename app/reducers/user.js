import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate = {
  loginInfo: null,
  checkError: null,
  createMemberError:null,
  getMemberSuccess:null,
  getMemberError:null,
  verificationFailureCount: 0,
  playerId: null,
  updatePlayerIdRes: null,
  deletePlayerIdRes: null
}

export const userReducer = createReducer(initialstate, {
  [types.CHECK_ERROR](state,action) {
    return Object.assign({}, state, {
      checkError: action.error
    })
  },
  [types.SET_NUMBER_INFO](state,action) {
    return Object.assign({}, state, {
      loginInfo: Object.assign({}, action.payload)
    })
  },
  [types.CREATE_MEMBER_ERROR](state,action) {
    return Object.assign({}, state, {
      createMemberError: action.error
    })
  },
  [types.GET_SINGLE_MEMBER](state,action) {
    return Object.assign({}, state, {
      getMemberSuccess: action.payload,
      getMemberError: action.error
    })
  },
  [types.VERIFICATION_FAILURE](state,action) {
    return Object.assign({}, state, {
      verificationFailureCount: action.payload,
    })
  },
  [types.RESET_VERIFICATION_FAILURE](state,action) {
    return Object.assign({}, state, {
      verificationFailureCount: initialstate.verificationFailureCount,
    })
  },
  [types.SET_PLAYER_ID](state,action) {
    return Object.assign({}, state, {
      playerId: action.payload,
    })
  },
  [types.UPDATE_PLAYER_ID](state,action) {
    return Object.assign({}, state, {
      updatePlayerIdRes: action.payload,
    })
  },
  [types.DELETE_PLAYER_ID](state,action) {
    return Object.assign({}, state, {
      deletePlayerIdRes: action.payload,
    })
  }
})
