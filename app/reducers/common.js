import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

var initialstate ={
  netwrokStatus: null,
  isOffline: false
}

export const commonReducer = createReducer(initialstate,{
  //switch network status in application
  [types.SET_NETWORK_STATUS](state,action) {
    return Object.assign({}, state, {
      netwrokStatus: action.data,
      isOffline: action.status
    })
  },
});
