import SplashScreen from '../components/splashScreen/SplashScreen'
import LoginScreen from '../components/login/LoginScreen'
import NointernetScreen from '../components/nointernet/NointernetScreen'

import Calender from'../components/calender/Calender'
import Dashboard from'../components/dashboard/Dashboard'
import Detail_Dashboard from'../components/dashboard/Detail_Dashboard'

import Opportunity from'../components/opportunity/Opportunity'
import Account from'../components/account/Account'
import Contact from '../components/contact/Contact'
import PageLead from '../components/lead'
import PageCalender from '../components/calender'

import report1 from '../components/report/report1'
import report2 from '../components/report/report2'
import report3 from '../components/report/report3'
import maps from '../components/report/maps'



import {
   createDrawerNavigator,createSwitchNavigator,createStackNavigator
} from 'react-navigation';

// const AppsDrawer = createDrawerNavigator({
//   Calender      : PageCalender,
//   Lead          : PageLead,
//   Opportunity   : Opportunity,
//   Account       : Account,
//   Contact       : Contact,
//   },{
//     defaultNavigationOptions:{
//       headerStyle: {
//         height: 40,
//         backgroundColor: '#3F51B5',
//         color: '#01275c',
//       }
//     }
//   }
// );

const AppsStack = createStackNavigator({

  Opportunity : {
    screen : Opportunity
  },

  report1 : {
    screen : report1
  },

  report2 : {
    screen : report2
  },

  report3 : {
    screen : report3
  },

  maps : {
    screen : maps
  },
  
  Login : {
    screen : LoginScreen
  },
});

const Mainstack = createSwitchNavigator({
  SplashScreen: { screen: SplashScreen},
  Login: { screen: LoginScreen},
  AppsStack:  AppsStack,
  NoInternet: { screen: NointernetScreen}
  },
  {
      initialRouteName: 'SplashScreen',
  }
);

export default Mainstack;
