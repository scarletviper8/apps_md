import React, { Component } from 'react'
import { BackHandler, AppState } from 'react-native'
import { NavigationActions,createAppContainer } from 'react-navigation'

import { connect } from 'react-redux'
import { ActionCreators } from '../actions'
import { bindActionCreators } from 'redux'
import MainStack from './mainstack'
import NavigationService from './NavigationService'

// import firebase from 'react-native-firebase';
// import OneSignal from 'react-native-onesignal';
let Navigation = createAppContainer(MainStack);

class AppWithNavigationState extends Component {
  constructor() {
    super()
    this.state = {
      appState:'',
      notification:null
    }
    this.isAppActive = false
  }

  componentWillMount() {
    AppState.addEventListener('change', this._handleAppStateChange);

    // OneSignal.inFocusDisplaying(0);
    // OneSignal.addEventListener('ids', this.onIds);
    // OneSignal.addEventListener('opened', this.onOpened);
    // OneSignal.addEventListener('received', this.onReceived);
  }


  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
  }

  componentWillUnmount(){
    AppState.removeEventListener('change', this._handleAppStateChange);
    // OneSignal.removeEventListener('received', this.onReceived);
    // OneSignal.removeEventListener('opened', this.onOpened);
    // OneSignal.removeEventListener('ids', this.onIds);
  }

  _handleAppStateChange = (nextAppState) => {
    this.setState({appState: nextAppState});
  }

  onReceived = (notification)=> {
    this.props.setNotification(true);
    console.log("Notification received: ", notification);
  }

  _handleAppStateChange = (nextAppState) => {
    this.setState({appState: nextAppState});
  }

  onOpened = (openResult) =>{
    const { payload } = openResult.notification
    const broadcastId = payload.additionalData ? payload.additionalData.broadcastID : ''

    if (this.state.appState.match(/inactive|background/)) {
      let route = 'Inbox'

      if (broadcastId) {
        this.props.setSelectedId(broadcastId)
        this.props.readBroadcast(broadcastId)
        route = 'MessageDetails'
      }

      this.props.dispatch(NavigationActions.navigate({ routeName: route }));
    } else{
      this.setState({ notification: openResult })
    }
  }

  onIds = (device) => {
    console.log('Device info: ', device);
    this.props.setPlayerId(device.userId)
  }



  onBackPress = () => {
    const { dispatch, navReducer } = this.props
    if (navReducer.index === 0) {
      return false
    }

    const routeName = navReducer.index > 0 ? navReducer.routes[navReducer.index - 1].routeName : ''
    const prevRoute = navReducer.index > 0 ? navReducer.routes[navReducer.index].routeName : ''

    if (routeName === 'Home') {
      // this.props.billboard()
      // this.props.memberMerchant()
      // this.props.merchantList()
    }

    if (prevRoute === 'Inbox') {
      // this.props.setNotification(false);
    }

    this.props.resetVerficationFailureCount()
    dispatch(NavigationActions.back())
    return true
  }

  render() {
    return (
      // <MainStack
      //   navigation={{
      //     dispatch: this.props.dispatch,
      //     state: this.props.navReducer,
      //   }}
      //   screenProps={{notification:this.state.notification}}
      //   />
      <Navigation ref={navigatorRef => {
        NavigationService.setTopLevelNavigator(navigatorRef);
      }}/>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    navReducer: state.navReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return Object.assign({dispatch: dispatch}, bindActionCreators(ActionCreators, dispatch))
}

export default connect(mapStateToProps, mapDispatchToProps)(AppWithNavigationState)
