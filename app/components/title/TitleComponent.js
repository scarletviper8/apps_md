import React, { PropTypes } from 'react';
import {
  AsyncStorage,
  Dimensions,
  Image,
  Button,
  Text,
  View,
  TouchableOpacity,
  Linking,
  ListView,
  ScrollView,
  StyleSheet,
  TextInput,
  ImageBackground,
  KeyboardAvoidingView,
  StackNavigator,
} from 'react-native';
import styled, {ThemeProvider} from "styled-components/native";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions'

const {height, width} = Dimensions.get('window');

const HeaderTitle = styled.Text`
text-align:center;
align-self:center;
color:#000000;
font-size:18px;
`;
const HeaderImage = styled.Image`
align-self:center;
width:${width * 0.3}px;
height:30px;
`;

class TitleComponent extends React.Component {
  render(){
    const { image, navigation, text,  merchantDetail } = this.props

    if (image) {
      return (<HeaderImage
        source={require('../../../assets/KartumiLogo_small.png')}
        resizeMode={'contain'}
        />)
      } else if (navigation && navigation.state.routeName === 'MerchantDetail' && merchantDetail) {
        return (<HeaderTitle>{merchantDetail[0].fullName}</HeaderTitle>)
      } else {
        return (<HeaderTitle>{text}</HeaderTitle>)
      }
    }
  }

  const mapStateToProps = (state, props) => {
    return {
      merchantDetail: state.merchantReducer.merchantDetail,

    };
  };

  const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(ActionCreators, dispatch);
  };

  export default connect(mapStateToProps, mapDispatchToProps)(TitleComponent)
