import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View, ScrollView, Platform, StyleSheet, Keyboard } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import { Item, Input, Form, Label, Button, Thumbnail, Text } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';


let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

class Account extends React.Component {
  static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text='Account'/>),
    headerLeft: (<HeaderLeft {...navigation}/>),
  })

  constructor() {
    super()
    this.state = { number: '', error: null, loading: false }
    
  }


  render() {
    return (
      <View style={styles.container}>
            <Text>Lead</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container :{
    flex:1,
  },
  bgimage:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    width:'100%',
    height:'100%'
  },
  logoStyle:{
    marginTop:70,
    marginBottom:80,
    alignItems:'center',
    justifyContent:'center',
  },
  form:{
    marginTop: -30,
    paddingLeft:10,
    paddingRight:30
  },
  inputStyle:{
    color: 'white',
    marginBottom: 6,
    fontSize: 14,
  },
  btn:{
     marginTop:26,
     paddingTop: 10,
     marginLeft:16,
     marginRight:16,
  },
  linearGradient: {
    padding:10,
    marginTop:20,
    alignItems:'center',
    marginLeft:16,
    marginRight:16,
    borderRadius: 5
  },
});

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Account)
