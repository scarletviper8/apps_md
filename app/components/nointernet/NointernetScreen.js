import React from 'react';
import { Image, View } from 'react-native'
import styled from "styled-components/native";

import { FormText } from '../commonstyle/CommonStyles';
import TitleComponent from '../title/TitleComponent';

// TODO: missing shadow for ios
const MainContainer = styled.View`
display:flex;
flex:1;
background-color:#FFFFFF;
border-radius:5;
elevation:2;
shadow-opacity: 0.15;
shadow-radius: 1px;
shadow-offset: 1px 1px;
margin:10px;
align-items:center;
justify-content:center;
`;

const InnerDetailContainer = styled.View`
align-items:center;
margin-top:3px;
`;

const InnerContainer = styled.View`
align-items:center;
margin-top:35px;
`;

const PlanText = styled.Text`
margin-left:3px;
font-size:12px;
color:#898989;
`;

export default class NointernetScreen extends React.Component {
  static navigationOptions = (navigation) => ({
    headerTitle:(<TitleComponent image={true} />),
    headerRight: (<View/>),
  })

  render(){
    return(
      <MainContainer>
        <Image  source={require('../../../assets/Illustration.png')} style={{width:130,height:148}}/>
        <InnerContainer>
          <FormText>Tidak Ada Koneksi Internet</FormText>
          <InnerDetailContainer>
            <PlanText>Koneksi internet Anda kurang baik.</PlanText>
            <PlanText>Mohon gunakan koneksi lain</PlanText>
          </InnerDetailContainer>
        </InnerContainer>
      </MainContainer >
    )
  }
}
