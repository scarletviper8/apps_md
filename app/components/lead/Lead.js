import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View, ScrollView, Platform, StyleSheet, Keyboard, Text } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';

import { Icon } from 'native-base';

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

const MainContainer = styled.View`
flex:1;
background-color:#F2F2F2;
`;

const Card = styled.TouchableOpacity`
    background-color:#ffffff;
    margin  : 5px;
    border-radius: 3px;
`
const Isi = styled.View`
    background-color:#ffffff;
    padding:3px;
    width: 90%;
    padding-left:10px;
`
const Detail = styled.View`
    align-items:center;
    width: 10%;
    margin: auto;
`
const Desc = styled.Text`
    font-family: Lato;
    color: #232323;
    font-size:13px;
`
const Tengah = styled.View`
    margin: auto;
    align-items:center;
`
const Status = styled.View`
    align-items:center;
    width: 13%;
`

class Lead extends React.Component {

static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text='Lead'/>),
    headerLeft: (<HeaderLeft {...navigation}/>),
  })

  oke = () =>{
    this.props.navigation.toggleDrawer()
  }
  
  componentDidMount() {
    
  }

  detail = () =>{
    // this.props.navigation.navigate('Leadview')
    this.props.tesdata()
  }

  render() {
    return (
      <MainContainer>
        <Card style={styles.bayangan} onPress={this.detail}>
            <View style={{flexDirection:'row',margin:1}}>
                {/* <Status>
                    <Tengah>  
                        <Text style={{fontSize:10,fontWeight:'bold',color:'red'}}>left1</Text>
                        <Text style={{fontSize:10,fontWeight:'bold',color:'red'}}>left2</Text>
                    </Tengah>
                </Status> */}
                <Isi>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>  
                        <Desc style={{fontWeight:'bold'}}>Nama </Desc>
                        <Desc style={{fontWeight:'bold'}}>Status Lead</Desc>
                    </View>
                    <Desc style={{fontWeight:'bold'}}>Company </Desc>
                    <Desc style={{color:'#666769'}}>Tanggal </Desc>
                </Isi>
                <Detail>
                    <Icon type='FontAwesome' name='chevron-right' style={{color:'#666769',fontSize:15}}/>
                </Detail>
            </View> 
            {/* <View style={{marginLeft:20,marginRight:20,marginTop:5,borderBottomColor: '#d1d1d1',borderBottomWidth: 1}}/> */}
        </Card>
      </MainContainer>
    )
  }
}

const styles = StyleSheet.create({
  bayangan: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
  }
});


const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Lead)
