import Lead from'./Lead'
import Leadview from './Leadview'

import {
    createStackNavigator
 } from 'react-navigation';

const PageLead = createStackNavigator({
    pageLead : Lead,
    Leadview : Leadview
  },{
    defaultNavigationOptions:{
      headerStyle: {
        height: 40,
        backgroundColor: '#007aff',
      },
      headerTintColor: '#ffffff',
    }
  });

  export default PageLead;