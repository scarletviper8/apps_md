import { createMaterialTopTabNavigator } from "react-navigation";

import LeadOverview from "./LeadOverview";
import LeadDetail from "./LeadDetail";
import Note from "../note/Note";

export default createMaterialTopTabNavigator({
    Overview   : { screen : LeadOverview},
    Detal      : { screen : LeadDetail},
    Note       : { screen : Note},
},
{
    tabBarOptions: {
        labelStyle: {
          fontSize: 12,
          fontWeight:'bold'
        },
        tabStyle: {
          padding: 0, margin:0,   //Padding 0 here
        },
        style: {
          backgroundColor: '#007aff',
        },
    }
     
})