import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View, ScrollView, Platform, StyleSheet, Keyboard, Text } from 'react-native';
import { NavigationActions,createAppContainer } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';
import Content from './Content';

import { Icon } from 'native-base';

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

const MainContainer = styled.View`
flex:1;
background-color:#F2F2F2;
`;

const Card = styled.TouchableOpacity`
    background-color:#ffffff;
    margin  : 5px;
    border-radius: 3px;
`
const Isi = styled.View`
    background-color:#ffffff;
    padding:3px;
    width: 90%;
    padding-left:10px;
`
const Detail = styled.View`
    align-items:center;
    width: 10%;
    margin: auto;
`
const Desc = styled.Text`
    font-family: Lato;
    color: #232323;
    font-size:13px;
`
const Tengah = styled.View`
    margin: auto;
    align-items:center;
`
const Status = styled.View`
    align-items:center;
    width: 13%;
`
let TabContent = createAppContainer(Content);

class Leadview extends React.Component {

static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text=''/>),
    // headerLeft: (<HeaderLeft {...navigation}/>),
  })

  oke = () =>{
    this.props.navigation.toggleDrawer()
  }

  detail = () =>{
    this.props.navigation.navigate('detail_lead')
  }

  render() {
    return (
      <MainContainer>
        <View style={{paddingLeft:20,backgroundColor:'#007aff'}}>
          <Desc style={{fontSize:15, fontWeight:"bold",color:'#ffffff'}}>Lead Name</Desc>
        </View>
        <View style={{paddingLeft:20,backgroundColor:'#007aff'}}>
          <Desc style={{fontSize:13, fontWeight:"bold",color:'#ffffff'}}>Nama Perusahaan</Desc>
        </View>
        <TabContent/>
      </MainContainer>
    )
  }
}

const styles = StyleSheet.create({
  bayangan: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
  }
});


const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Leadview)
