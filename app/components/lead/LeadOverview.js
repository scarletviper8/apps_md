import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions,TouchableOpacity, Image, View, ScrollView, Platform, StyleSheet, Keyboard, Text } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';
import Content from './Content';

import { Icon } from 'native-base';

import StepIndicator from 'react-native-step-indicator';

import Tombol from '../tombol/Tombol'

const labels = ["New","Contacted","Working","Converted"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
  labelAlign: 'center',
}

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

const MainContainer = styled.View`
flex:1;
background-color:#ffffff;
`;

const Card = styled.TouchableOpacity`
    background-color:#ffffff;
    margin  : 5px;
    border-radius: 3px;
`
const Isi = styled.View`
    background-color:#ffffff;
    padding:3px;
    width: 90%;
    padding-left:10px;
`
const Detail = styled.View`
    align-items:center;
    width: 10%;
    margin: auto;
`
const Desc = styled.Text`
    font-family: Lato;
    color: #232323;
    font-size:13px;
`
const Tengah = styled.View`
    margin: auto;
    align-items:center;
`
const Status = styled.View`
  align-items:center;
  width: 13%;
`
const Group = styled.TouchableOpacity`
  padding:5px;
  border-bottom-color:#d1d1d1;
  border-bottom-width:1px;
` 
const Label = styled.Text`
  font-size:11px
` 
const Field = styled.Text`
  font-size:13px;
  font-weight: bold;
  color:#000000;
`
const Title = styled.Text`
  background-color:#e8e8e8;
  color:#5e5e5e;
  padding:5px;
  font-weight: bold;
  font-size:15px;
` 

class LeadOverview extends React.Component {
  // constructor() {
  //   this.state = {
  //       currentPosition: 0
  //   }
  // }

  static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text=''/>),
    // headerLeft: (<HeaderLeft {...navigation}/>),
  })

  oke = () =>{
    this.props.navigation.toggleDrawer()
  }

  render() {
    return (
      <MainContainer>
        <Tombol/>
        <View style={{height:5}} />
        <View >
          <StepIndicator
          stepCount={4} 
          customStyles={customStyles}
          currentPosition={1}
          labels={labels}
          />
        </View>
        <Title>Next Step</Title>
        <Group style={{flexDirection:'row'}}>
          <Isi>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Field>Report Visit</Field>
              <Label>Tanggal</Label>
            </View>
            <Label>Description</Label>
          </Isi>
          <Detail>
            <Icon type='FontAwesome' name='chevron-right' style={{color:'#666769',fontSize:15}}/>
          </Detail>
        </Group>
        <Title>Past Activity</Title>
        <Group style={{flexDirection:'row'}}>
          <Isi>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Field>Report Visit</Field>
              <Label>Tanggal</Label>
            </View>
            <Label>Description</Label>
          </Isi>
          <Detail>
            <Icon type='FontAwesome' name='chevron-right' style={{color:'#666769',fontSize:15}}/>
          </Detail>
        </Group>
      </MainContainer>
    )
  }
}

const styles = StyleSheet.create({
  bayangan: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
  }
});


const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LeadOverview)
