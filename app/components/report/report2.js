import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
import { Item, Input, Form, Label, Thumbnail, Card, Icon, Button } from 'native-base';
import Line from '../help/line';


import { Text, Picker} from "native-base";

export default class Report1 extends Component {

  static navigationOptions = {
    title: 'Report Sales Per Jam',
  };


  constructor(props) {
    super(props);
    this.state = {
      tableHead: ['ID',     'BARCODE',       'GROUP', 'NAMA',                   'SUPPLIER', 'Qty', 'BRUTO',   'DISC', 'L.PRICE',  'HPP',      'Mrg Rp', 'Mrg %'],
      tableData: [
                 ['127',    '8997034360090', '10001', 'BERAS LUMBUNG PADI 5KG', 'G0028',    '24',  '1308000', '0',     '1308000', '1240707',  '67292',  '5.144675'],
                 ['1379',   '8993496110525', '10001', 'SANIA BERAS 5 KG',       'G0028',    '22',  '1201200', '0',     '1201200', '1957827',  '66993',  '5.577181'],
                 ['2653',   '8993496110556', '10001', 'BERAS FORTUNE 5 KG',     'G0028',    '38',  '2071000', '0',     '2071000', '23909',    '113172', '5.464614'],
                 ['101927', '101927',        '10001', 'JANTRA BERAS HITAM',     'G0028',    '1',   '25500',   '0',     '25500',   '423857',   '1590',   '6.235999']
      ],

      widthArr : [ 80, 150, 80, 200, 150, 60, 120, 60, 100, 100, 100, 100, ], 

      dateStart: "17-08-2019",
      dateEnd: "20-08-2019",
    }
  }
 
  Love(itemValue){
    this.setState({tableHead: ['GROUP']});
    this.setState({tableData: ['10001']});
  }

  render() {
    const state = this.state;
    return (
      <ScrollView>
      <View style={styles.container}>

        <View style={[styles.Tampilkan]}>
            <View>
              <Text style={[styles.textTampilan]}>TAMPILKAN BERDASARKAN</Text>
            </View>
        </View>

        <View style={{flexDirection : 'row'}}>
          <View>
            <View style={{height :40, width : 250, backgroundColor : '#e6e6e6', borderRadius: 5, marginBottom: 20, marginTop : 10}}>
              <Picker
                   selectedValue   ={this.state.language}
                   style           ={{height: 40, width: 250,}}
                   onValueChange   ={(itemValue, itemIndex) =>
                     this.setState({language: itemValue})
                  }>
                  
                  <Picker.Item label="Per Toko"     value="PerToko" />
                  <Picker.Item label="Per Kasir"    value="PerKasir" />
                  
              </Picker>
            </View>
            <View style={[styles.Keterangan]}>
              <Item regular>
                <Input placeholder='Keterangan' />
              </Item>
            </View>
          </View>
        </View>
        <Line/>

        {/* <View style={[styles.Tampilkan]}>
            <View>
              <Text style={[styles.textTampilan]}>PERIODE</Text>
            </View>
        </View>

        <View style={{flexDirection: 'row', marginBottom :10}}>
            <View style={[styles.DariTanggal]}>
              <Text style={{fontWeight: 'bold',}}>Dari Tanggal</Text>
            </View>
    
          <View style={[styles.Tanggal]}>
          <DatePicker
                style={{width: 150}}
                date={this.state.dateStart}
                mode="date"
                placeholder="select date"
                format="DD-MM-YYYY"
                minDate="17-08-2001"
                maxDate="17-08-2030"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position  : 'absolute',
                    left      : 0,
                    top       : 4,
                    marginLeft: 0,
                    
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(dateStart) => {this.setState({dateStart: dateStart})}}
              />
          </View>
        </View>

        <View style={{flexDirection: 'row', marginBottom : 10, marginTop : 20}}>
            <View style={[styles.SampaiTanggal]}>
              <Text style={{fontWeight: 'bold',}}>Sampai Tanggal</Text>
            </View>
    
          <View style={[styles.Tanggal]}>
          <DatePicker
                style={{width: 150}}
                date={this.state.dateEnd}
                mode="date"
                placeholder="select date"
                format="DD-MM-YYYY"
                minDate="17-08-2001"
                maxDate="17-08-2030"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position  : 'absolute',
                    left      : 0,
                    top       : 4,
                    marginLeft: 0,
                    
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(dateEnd) => {this.setState({dateEnd: dateEnd})}}
              />
          </View>
        </View> */}

        <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
          <TouchableOpacity>
            <View style={[styles.Button]}>
              <Button iconRight style={{backgroundColor : '#00abd1'}}>
                <Icon name='search' style={{color:'white', marginLeft : 10}}/>
                <Text style={{marginLeft : -25}}>Cari</Text>
              </Button>
            </View>
          </TouchableOpacity>
       
          <View styles={[styles.Button2]}>
            <Button iconRight style={{backgroundColor : '#00abd1', marginTop : 10}}>
                <Icon name='cloud-download' style={{color:'white', marginLeft : 10}}/>
                <Text style={{marginLeft :-20}}>Unduh Table</Text>
              </Button>
          </View>
        </View>
        

        <ScrollView horizontal={true}>  
        <Table style={{height : 200}} borderStyle={{borderWidth: 2, borderColor: 'white'}}>
          <Row 
              data      ={state.tableHead}
              widthArr  ={state.widthArr} 
              style     ={styles.head} 
              textStyle ={styles.textHead}/>

          <Rows 
              data      ={state.tableData}
              widthArr  ={state.widthArr}
              style     ={[styles.row]}              
              textStyle ={styles.textBody}/>
        </Table>
        </ScrollView>
      </View>
      </ScrollView>
    )
  }
}
 
const styles = StyleSheet.create({
  container   : { flex: 1, padding: 16, paddingTop: 10, backgroundColor: '#fff' },
  head        : { height: 40, backgroundColor: '#89a7e8' },
  row         : { height : 35, backgroundColor : '#ebebeb'},
  textBody    : { margin: 6, },
  textHead    : { fontSize: 16, fontWeight : 'bold', margin : 6, textAlign :'center', color : 'white'},
  DariTanggal   : { marginLeft : 5, marginRight: 20, marginTop : 7, width : 120},
  SampaiTanggal : { marginLeft : 5, marginRight: 20, marginTop : 7, marginBottom : 20, width : 120},
  Tampilkan     : { flexDirection: 'row', marginBottom: 10, fontSize : 16, fontWeight : 'bold', backgroundColor: '#89a7e8', height : 35},
  textTampilan  : { fontSize : 17, fontWeight : 'bold', textAlign : 'center', marginTop: 5,  color : 'white', width : 330},
  Button        : { width : 130, marginLeft : 2, marginBottom : 5, marginTop : 10},
  Button2       : { width : 130, marginLeft : 2, marginBottom : 5, marginTop : 10},
  Keterangan    : { width : 250, height : 50, marginBottom : 20, borderRadius : 10}


});