import React, { Component } from 'react';
import StarRating from 'react-native-star-rating';
 
class ratingRead extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      starCount: 2,
     
    };
  }
 
  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
 
  render() {
    return (
      <StarRating
        disabled={true}
        maxStars={5}
        fullStarColor='#FFD700'
        starSize={25}
        starStyle={{padding : 3}}
        rating={this.props.Value}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
      
      />
    );
  }
}
 
export default ratingRead