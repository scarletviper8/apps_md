import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View, ScrollView, Platform, StyleSheet, Keyboard, TouchableOpacity } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'
import { Icon } from 'react-native-elements'

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';

import { Item, Input, Form, Label, Thumbnail, Text, Card, Button } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

class Opportunity extends React.Component {
  
    static navigationOptions = {
      title: 'Laporan SRM',
    };

  constructor() {
    super()
    this.state = { number: '', error: null, loading: false }
    
  }

  _onPressButton() {
    this.props.navigation.navigate('report1')
  }

  signin = () => {
    this.props.navigation.navigate('LoginScreen');
  }

  render() {
    return (
      <ScrollView>
      <View style={styles.container}>
      <Image style={styles.bgimage} source={require('../../../assets/images/bg.png')}/>

          <View style={[styles.viewIcon]}> 
            
              <Card style={{height:130, borderRadius :20}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('report1')}>
                <View style={{flexDirection: 'row', marginTop :10}} >
                  <View style={{flexDirection: 'column',}}> 
                    <Image source={require('../../../assets/logo/logoreport1.png')} style={[styles.icon]} />
                  </View>
                  <View style={{flexDirection: 'column', justifyContent: 'center',}}>
                    <Text style={[styles.iconTitle]}>Report Top Item</Text>
                  </View>
                </View>
                </TouchableOpacity>
              </Card>
         
              <Card style={{height:130,  borderRadius :20}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('report2')}>
                  <View style={{flexDirection: 'row', marginTop:10}} >
                    <View style={{flexDirection: 'column',}}> 
                      <Image source={require('../../../assets/logo/logoreport2.png')} style={[styles.icon]} />
                    </View>
                    <View style={{flexDirection: 'column', justifyContent: 'center',}}>
                      <Text style={[styles.iconTitle]}>Report Sales Per Jam</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </Card>
          

         
              <Card style={{height:130,  borderRadius :20}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('report3')}>
                  <View style={{flexDirection: 'row', marginTop:10}} >
                      <View style={{flexDirection: 'column',}}> 
                        <Image source={require('../../../assets/logo/logoreport3.png')} style={[styles.icon]} />
                      </View>
                      <View style={{flexDirection: 'column', justifyContent: 'center',}}>
                        <Text style={[styles.iconTitle]}>Report Sales</Text>
                      </View>
                  </View>
                  </TouchableOpacity>
              </Card>

              <Card style={{height:130,  borderRadius :20}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('maps')}>
                  <View style={{flexDirection: 'row', marginTop:10}} >
                      <View style={{flexDirection: 'column',}}> 
                        <Image source={require('../../../assets/logo/logoreport3.png')} style={[styles.icon]} />
                      </View>
                      <View style={{flexDirection: 'column', justifyContent: 'center',}}>
                        <Text style={[styles.iconTitle]}>Maps</Text>
                      </View>
                  </View>
                  </TouchableOpacity>
              </Card>
          </View>

          <View style={{flexDirection: 'row',}}>
          
          <View style={[styles.viewButton]}>
            <View style={[styles.Button]}>
              <TouchableOpacity>
                <Button iconRight style={{backgroundColor : '#00abd1', marginTop : 10,  width : 200}}>
                <Icon
                    name='exit-to-app'
                    color='white'
                    iconStyle={{marginLeft : 15, width : 50}}  />
                  <Text style={{marginLeft : 10, fontSize: 15}}>LOG OUT</Text>
                </Button>
                </TouchableOpacity>
              </View>
          </View>
         
        </View>
          
      </View>
      </ScrollView>
      

      

      
    )
  }
}

const styles = StyleSheet.create({
  container :{
    flex:1,
    justifyContent : 'flex-start',
    height : 640,
    
  },

  bgimage:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    width:'100%',
    height:'100%'
  },

  viewIcon : {
    flexDirection: 'column', 
    justifyContent : 'space-between',
    marginTop : 20,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 1,
    borderColor: 'black',
    borderStyle: 'solid',
  },

  icon:{
    width:100,
    height:100,
    marginTop   : 5,
    marginLeft  : 10,
    marginRight : 10
  },

  iconTitle: {
    textAlign : 'right',
    fontSize: 20,
    fontWeight: 'bold', 
  },

  desc: {
    textAlign : 'left',
    fontSize: 15,
    fontStyle : 'italic'
  },
  
  viewButton : { width : 300, height : 80, marginLeft : 80, marginBottom : 5, marginTop : 80},
  
  Button : { width : 300, fontSize : 30}

});

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Opportunity)
