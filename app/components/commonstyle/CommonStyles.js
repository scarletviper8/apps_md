import React from "react";
import styled from "styled-components/native";

export const BackgroundView = styled.Image`
  flex:1;
`;
export const ProfileDropdownBackground = styled.View`
background-color:#FFFFFF;
border-radius:5px;
padding-left:15px;
padding-right:15px;
border-color:#000000;
width:290px;
border-width:1px;
`;

export const ProfileTextinputBackground = styled.View`

`;
export const DarkborderinputBackground = styled.View`
background-color:#FFFFFF;
border-radius:5px;
border-color:#0085BE;
border-width:2.5px;
width:40px;
margin:5px;
`;
export const TextinputStyle = styled.TextInput`
min-width:290px;
font-size:15px;
font-family:${props => props.theme.regular};
border: 1px solid black;
border-radius:5px;
padding:15px;
`;

export const CodeinputStyle = styled.TextInput`
height:60px;
width:35px;
font-size:20px;
text-align:center;
`;

export const ItemContainer = styled.View`
  margin-top:15px;
`;

export const ButtonText = styled.Text`
color:#FFFFFF;
font-size:16px;
font-family:${props => props.theme.regular};
`;

export const FormText = styled.Text`
color:#148FC3;
font-size:18px;
text-align:center;
font-family:${props => props.theme.bold};
`;
export const WelcomeText = styled.Text`
color:rgba(0, 133, 191, 1);
font-size:24px;
margin-top:25px;
text-align:center;
font-family:${props => props.theme.bold};
`;

export const ValidationText = styled.Text`
color:rgba(209, 3, 28, 1);
font-size:13px;
text-align:center;
font-family:${props => props.theme.regular};
`;
export const SimpleText = styled.Text`
font-size:15px;
text-align:center;
color:#808080;
margin-bottom:20px;
font-family:${props => props.theme.bold};
`;
export const CardContainer = styled.View`
background-color:#FFFFFF;
border-radius:5px;
elevation:2;
shadow-opacity: 0.15;
shadow-radius: 1px;
shadow-offset: 1px 1px;
`;
export const RoundContainer = styled.View`
background-color:#FFFFFF;
border-width: 0.7px;
border-radius: 50;
border-color: #D2D2D2;
elevation:5;
width:85px;
height:85px;
margin:10px;
shadow-opacity: 0.45;
shadow-radius: 3px;
shadow-offset: 0px 1px;
`;
export const GreyBackground = styled.ScrollView`
  background-color:#F2F2F2;
`;
export const PlaceText = styled.Text`
color:#4A4A4A;
font-size:15px;
font-family:${props => props.theme.bold};
`;

export const GrayText = styled.Text`
color:rgba(150, 150, 150, 1);
text-align:center;
font-size:14px;
margin-left:5px;
margin-right:5px;
font-family:${props => props.theme.regular};
`;

export const TitleImage = styled.Image`
  width:250px;
  height:80px;
`;
