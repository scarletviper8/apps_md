import React from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import { Item, Input, Form, Label, Button, Thumbnail, Text,Icon } from 'native-base';

class Home extends React.Component {

  static navigationOptions = {
    drawerLabel: 'Dashboard',
  };

oke = () =>{
  this.props.navigation.toggleDrawer();
}

  render() {
    return (
      <View>
            <Text>Dashboard</Text>
            <Button block onPress={this.oke}>
              <Text>Sign In</Text>
            </Button>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
