import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View,Text, ScrollView, Platform, StyleSheet, Keyboard } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';


let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

class LoginScreen extends React.Component {
  static navigationOptions = {
    headerVisible:false,
    headerStyle:{
      width:0,
      height:0,
    },
  }

  constructor() {
    super()
    this.state = { number: '', error: null, loading: false }
    
  }

  componentWillReceiveProps() {

  }

  render() {
    return (
      // <ImageBackground source={require('../../../assets/images/bg.png')} style={styles.BgContainer}>
      //   <View>
      //     <SplashLogoImage
      //       source={require('../../../assets/logo/logo.png')}
      //       resizeMode={'contain'}
      //       />
      //   </View>
      //   <View>
      //   </View>
      // </ImageBackground>'
      <Container>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input />
            </Item>
          </Form>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  BgContainer :{
    flex:1,
    width:null,
    height:null,
    justifyContent:'center',
    alignItems:'center'
  },
  form :{
    margin:'10',
  }


});

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
