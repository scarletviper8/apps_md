import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View, ScrollView, Platform, StyleSheet, Keyboard } from 'react-native';
import { NavigationActions,StackActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import { Item, Input, Form, Label, Button, Thumbnail, Text } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

class LoginScreen extends React.Component {
  constructor() {
    super()
    this.state = { number: '', error: null, loading: false }
    
  }

  signin = () => {
    // const resetAction = StackActions.reset({
    //   index: 0,
    //   actions: [NavigationActions.navigate({ routeName: 'Home' })],
    // });
    // this.props.navigation.dispatch(resetAction);
    this.props.navigation.navigate('AppsStack');
  }




  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.bgimage} source={require('../../../assets/images/bg.png')}/>
        <View style={styles.logoStyle}>
          <Thumbnail  style={{resizeMode : 'contain', width : 400}} square large source={require('../../../assets/logo/logo3.png')} />
        </View>
        <Form style={styles.form}>
          <Item floatingLabel>
            <Label><Text style={styles.inputStyle}>Username</Text></Label>
            <Input style={styles.inputStyle}/>
          </Item>  
          <Item floatingLabel>
            <Label><Text style={styles.inputStyle}>Password</Text></Label>
            <Input style={styles.inputStyle} secureTextEntry={true}/>
          </Item>
          <Button block style={styles.btn} onPress={this.signin}>
            <Text>Sign In</Text>
          </Button> 
          {/* <LinearGradient colors={['#4c669f', '#3b5998', '#192f6a']} style={styles.linearGradient}>
            <Text style={{color:'white'}}>
              Sign In
            </Text>
          </LinearGradient> */}
        </Form>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container :{
    flex:1,
  },
  bgimage:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    width:'100%',
    height:'100%'
  },
  logoStyle:{
    marginTop:70,
    marginBottom:80,
    alignItems:'center',
    justifyContent:'center',
  },
  form:{
    marginTop: -30,
    paddingLeft:10,
    paddingRight:30
  },
  inputStyle:{
    color: 'white',
    marginBottom: 6,
    fontSize: 14,
  },
  btn:{
     marginTop:26,
     paddingTop: 10,
     marginLeft:25,
     marginRight:16,
     backgroundColor : '#00abd1'
    
  },
  linearGradient: {
    padding:10,
    marginTop:20,
    alignItems:'center',
    marginLeft:16,
    marginRight:16,
    borderRadius: 5
  },
});

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)
