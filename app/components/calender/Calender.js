import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import {Agenda} from 'react-native-calendars';
import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';

export default class Calender extends Component {

  static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text='Calender'/>),
    headerLeft: (<HeaderLeft {...navigation}/>),
  })

  constructor(props) {
    super(props);
    this.state = {
      items: {},
      date_now: '2019-01-01',
    };
  }

  componentWillMount() {
    var tgl_now = new Date().toISOString()
    var tgl_sekarang =  tgl_now.slice(0,10);

    var d = new Date();
    var year = d.getFullYear();
    var month = ("0" + (d.getMonth() + 1)).slice(-2);
    var day = d.getDate()+1;
    this.setState({
        date_now: year+'-'+month+'-'+day
    });
    console.log(year+'-'+month+'-'+day)
  }

  loadItems(day) {
    console.log(day);
    setTimeout(() => {
      for (let i = -15; i < 85; i++){
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            this.state.items[strTime].push({
              name: 'Item for ' + strTime,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
    // console.log('---satu---');  
    console.log(this.state.items);
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
      this.setState({
        items: newItems
      });
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
    // console.log(this.state.items);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, {height: item.height}]}><Text>{item.name}</Text></View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>This is empty date!</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    // console.log('---------------------');
    // console.log(r1)
    // console.log('---------------------');
    // console.log(r2)
    // console.log('---------------------');
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }

  render() {
    return (
      <Agenda
        items={this.state.items}
        loadItemsForMonth={this.loadItems.bind(this)}
        selected={this.state.date_now}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        // markingType={'period'}
        // markedDates={{
        //  '2017-05-08': {textColor: '#666'},
        //  '2017-05-09': {textColor: '#666'},
        //  '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
        //  '2017-05-21': {startingDay: true, color: 'blue'},
        //  '2017-05-22': {endingDay: true, color: 'gray'},
        //  '2017-05-24': {startingDay: true, color: 'gray'},
        //  '2017-05-25': {color: 'gray'},
        //  '2017-05-26': {endingDay: true, color: 'gray'}}}
        // monthFormat={'yyyy'}
        // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
        // renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
      />
    );
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  }
});