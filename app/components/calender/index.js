
import Calender from'./Calender'
import {
    createStackNavigator
 } from 'react-navigation';


const PageCalender = createStackNavigator({
    pageCalender : Calender,
  },{
    defaultNavigationOptions:{
      headerStyle: {
        height: 40,
        backgroundColor: '#007aff',
        color: '#01275c',
      }
    }
  });

  export default PageCalender;