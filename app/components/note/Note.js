import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View,
  TouchableWithoutFeedback, ScrollView, Platform, StyleSheet, Keyboard, Text } from 'react-native';
import { NavigationActions } from 'react-navigation'
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';

import { Icon } from 'native-base';

let {height, width} = Dimensions.get('window')

height = (height - 20)

const logoHeight = (height / 2.5)

const contentHeight = height - logoHeight

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

const MainContainer = styled.View`
flex:1;
background-color:#ffffff;
`;

const Card = styled.TouchableOpacity`
    background-color:#ffffff;
    margin  : 5px;
    border-radius: 3px;
`
const Isi = styled.View`
    background-color:#ffffff;
    padding:3px;
    width: 90%;
    padding-left:10px;
`
const Detail = styled.View`
    align-items:center;
    width: 10%;
    margin: auto;
`
const Desc = styled.Text`
    font-family: Lato;
    color: #232323;
    font-size:13px;
`
const Tengah = styled.View`
    margin: auto;
    align-items:center;
`
const Status = styled.View`
    align-items:center;
    width: 13%;
`
const Group = styled.TouchableOpacity`
  padding:5px;
  padding-left:10px;
  border-bottom-color:#d1d1d1;
  border-bottom-width:1px;
` 
const Label = styled.Text`
  font-size:11px
` 
const Field = styled.Text`
  font-size:13px;
  font-weight: bold;
  color:#000000;
`
const Title = styled.Text`
  background-color:#e8e8e8;
  color:#5e5e5e;
  padding:5px;
  font-weight: bold;
  font-size:15px;
`

class Note extends React.Component {

static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text=''/>),
    // headerLeft: (<HeaderLeft {...navigation}/>),
  })

  oke = () =>{
    this.props.navigation.toggleDrawer()
  }

  render() {
    return (
      <MainContainer>
        <TouchableWithoutFeedback>
          <View style={[styles.button,styles.pay]}>
            <Icon style={{fontSize: 20, color: '#ffffff'}} type="MaterialIcons" name='add' />
          </View>
        </TouchableWithoutFeedback>
        <Group style={{flexDirection:'row'}}>
          <Isi>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
              <Field>Subject</Field>
              <Label>Tanggal</Label>
            </View>
            <Label>Description</Label>
          </Isi>
          <Detail>
            <Icon type='FontAwesome' name='chevron-right' style={{color:'#666769',fontSize:15}}/>
          </Detail>
        </Group>
      </MainContainer>
    )
  }
}

const styles = StyleSheet.create({
  bayangan: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
  },
  button:{
    width:40,
    height:40,
    alignItems:"center",
    justifyContent:"center",
    shadowColor:"#333",
    shadowOpacity: .1,
    shadowOffset:{x:2,y:1},
    shadowRadius:2,
    borderRadius:30,
    position:"absolute",
    bottom: 20,
    right: 20,
  },
  pay:{
    backgroundColor:"#007aff",
  }

});


const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Note)
