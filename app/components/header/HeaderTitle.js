import React from 'react';
import { View, StyleSheet,TouchableOpacity, Text } from 'react-native';
import styled from "styled-components/native";

import { Icon } from 'native-base';

const Container = styled.View`
    padding-left:10px;
`;

class HeaderTitle extends React.Component {

  oke = () =>{
    this.props.navigation.toggleDrawer();
  }

  render() {
    return (
      <View>
        <Text style={{fontSize: 20,fontWeight:"bold",color:'#ffffff'}}>{this.props.text}</Text>
      </View>
    )
  }
}
export default HeaderTitle
