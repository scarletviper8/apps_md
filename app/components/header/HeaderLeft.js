import React from 'react';
import { View, StyleSheet,TouchableOpacity, Text } from 'react-native';
import styled from "styled-components/native";

import { Icon } from 'native-base';

const Container = styled.TouchableOpacity`
    padding-left:20px;
`;

class HeaderLeft extends React.Component {

  oke = () =>{
    this.props.navigation.toggleDrawer();
  }

  detail = () =>{
    this.props.navigation.navigate('detail_dashboad')
  }

  render() {
    return (
      <Container onPress={this.oke}>
        <Icon style={{fontSize: 20, color: '#ffffff'}} name='menu' />
      </Container>
    )
  }
}

export default HeaderLeft
