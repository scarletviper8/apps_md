import React from 'react'
// import { GoogleSignin } from 'react-native-google-signin';
import NetInfo from "@react-native-community/netinfo"
import {   Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationActions,StackActions } from 'react-navigation'
import styled from "styled-components/native"

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

const { width, height } = Dimensions.get('window')

const ImageBackgroundStyle = styled.ImageBackground`
display: flex;
flex:1;
align-items:center;
justify-content:center;
padding-right:30;
padding-left:30;
`;

const SplashLogoImage = styled.Image`
width: ${width * 0.5}px;
`

class SplashScreen extends React.Component {
  static navigationOptions = {
    headerVisible: false,
    headerStyle:{ width:0, height:0 }
  }

  componentDidMount() {
    this.loadInitialState()
    // GoogleSignin.configure({
    //   iosClientId:'968232233121-mjvpb8ssml9mkdjsafhkvu6htla9o29u.apps.googleusercontent.com'
    // })
    console.log('oke');
  }

  loadInitialState = () => {
    let nextScreen = 'Login';
    const storage = AsyncStorage.getItem('Kuma_loginInfo')
    // AsyncStorage.getItem('Kuma_loginInfo').then((data) => {
    //   if (data && data.length > 0) {
    //     const loginData = JSON.parse(data)
    //     nextScreen = loginData.verified ? loginData.memberId ? 'Home' : 'Register' : 'Login'
    //     this.props.setLoginInfo(loginData)
    //   }
    // })

    if (storage && storage.length > 0) {
      const loginData = JSON.parse(storage)
      nextScreen = loginData.verified ? loginData.memberId ? 'Home' : 'Register' : 'Login'
      this.props.setLoginInfo(loginData)
    }

    setTimeout(() => {
      // const resetAction = StackActions.reset({
      //   index: 0,
      //   actions: [NavigationActions.navigate({ routeName: nextScreen })],
      // });
      // this.props.navigation.dispatch(resetAction);
      this.props.navigation.navigate(nextScreen);
      
      NetInfo.fetch().then((connectionInfo) => {
        this.props.setNetworkStatus(connectionInfo.type)
        if (connectionInfo.type === 'none') {
          this.props.navigation.navigate('NoInternet')
        }
      });
      NetInfo.addEventListener(this.handleConnectivityChange);
      
    }, 1000);
  }

  handleConnectivityChange = (connectionInfo) => {
    this.props.setNetworkStatus(connectionInfo.type)
    if (connectionInfo.type === 'none') {
      this.props.navigation.navigate('NoInternet')
    }
  }

  render () {
    return (
      <ImageBackgroundStyle source={require('../../../assets/images/bg.png')}>
        <SplashLogoImage
          source={require('../../../assets/logo/logo3.png')}
          resizeMode={'contain'}
          />
      </ImageBackgroundStyle>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isOffline: state.commonReducer.isOffline
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen)
