import React, { PropTypes } from 'react';
import { ImageBackground, AsyncStorage, Dimensions, Image, View,
  TouchableWithoutFeedback, ScrollView, Platform, StyleSheet, Keyboard, Text } from 'react-native';
import { NavigationActions } from 'react-navigation'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'

import { Icon } from 'native-base';

class Tombol extends React.Component {

  oke = () =>{
    this.props.navigation.toggleDrawer()
  }

  render() {
    return (
        <TouchableWithoutFeedback>
          <View style={[styles.button,styles.pay]}>
            <Icon style={{fontSize: 20, color: '#ffffff'}} type="MaterialIcons" name='add' />
          </View>
        </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  button:{
    width:40,
    height:40,
    alignItems:"center",
    justifyContent:"center",
    shadowColor:"#333",
    shadowOpacity: .1,
    shadowOffset:{x:2,y:1},
    shadowRadius:2,
    borderRadius:30,
    position:"absolute",
    bottom: 20,
    right: 20,
  },
  pay:{
    backgroundColor:"#007aff",
  }
});


const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Tombol)
