import React from 'react';
import {  Button,Platform,StyleSheet,TextInput,Text,View,Image,
  KeyboardAvoidingView,BackHandler,ScrollView,TouchableOpacity } from 'react-native';
import styled from "styled-components/native";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../../actions'
import { Grid, Section, Block } from 'react-native-responsive-layout';

import HeaderLeft from '../header/HeaderLeft';
import HeaderTitle from '../header/HeaderTitle';

const MainContainer = styled.View`
flex:1;
background-color:#F2F2F2;
`;

const Card = styled.TouchableOpacity`
    background-color:#ffffff;
    margin  : 5px;
    border-radius: 3px;
`
const Isi = styled.View`
    background-color:#ffffff;
    padding:3px;
    width: 80%;
`
const Detail = styled.View`
    align-items:center;
    width: 7%;
    margin: auto;
`
const Desc = styled.Text`
    font-family: Lato;
    color: #232323;
`
const Tengah = styled.View`
    margin: auto;
    align-items:center;
`
const Status = styled.View`
    align-items:center;
    width: 13%;
`

class Dashboard extends React.Component {

  static navigationOptions = (navigation) => ({
    headerTitle:(<HeaderTitle text='Dashboard'/>),
    headerLeft: (<HeaderLeft {...navigation}/>),
  })

  oke = () =>{
    this.props.navigation.toggleDrawer();
  }

  detail = () =>{
    this.props.navigation.navigate('detail_dashboad')
  }

  render() {
    return (
      <MainContainer>
        <Card style={styles.bayangan} onPress={this.detail}>
            <View style={{flexDirection:'row',margin:1}}>
                <Status>
                    <Tengah>  
                        <Text style={{fontSize:10,fontWeight:'bold',color:'red'}}>left1</Text>
                        <Text style={{fontSize:10,fontWeight:'bold',color:'red'}}>left2</Text>
                    </Tengah>
                </Status>
                <Isi>
                    <View style={{flexDirection:'row',justifyContent:'space-between'}}>  
                        <Desc>Nama </Desc>
                        <Text>Lead</Text>
                    </View>
                    <Desc>Tanggal </Desc>
                    <Desc>Desc. 123 few f ewf wef we f ewfwef ew f wef we fw ef wef we  fw ef ewf ewf  ewfwe f</Desc>
                </Isi>
                <Detail>
                    <Text>a</Text>
                </Detail>
            </View> 
            {/* <View style={{marginLeft:20,marginRight:20,marginTop:5,borderBottomColor: '#d1d1d1',borderBottomWidth: 1}}/> */}
        </Card>
      </MainContainer>
    )
  }
}

const styles = StyleSheet.create({
  bayangan: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    
    elevation: 5,
  }
});

const mapStateToProps = (state) => {
  return {
    loginInfo: state.userReducer.loginInfo,
    error: state.userReducer.checkError
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
