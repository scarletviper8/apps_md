export const Constants = {
  LOGIN: {
    REQUIRED: 'Please enter your phone number'
  },
  VERIFICATION: {
    INVALID: 'Invalid code',
    REQUIRED: 'Please enter verification code'
  },
  REGISTRATION: {
    NAME: 'Please enter name',
    GENDER: 'Please enter gender',
    PHONE:'Please enter phone',
    EMAIL: 'Please enter email',
    DATEOFBIRTH: 'Please enter birth date',
    VALIDEMAIL:'Please enter valid email'
  },
  COUNTRYCODE:'+62',
  GOOGLE: 'google',
  FACEBOOK: 'facebook',
  DOMAIN:'http://www.kartukami.com'
}
