import CoreApi from './core'

export default {
  merchantList: (values, cb) => {
    let data = {
      url: `corporate/merchantfront`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  memberMerchant: (values, cb) => {
    let data = {
      url: `member/merchants?id=${values.id}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantCity: (values, cb) => {
    let data = {
      url: `corporate/city`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantBanner: (values, cb) => {
    let data = {
      url: `corporate/banner?userId=${values.userId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  points: (values, cb) => {
    let data = {
      url: `member/points?id=${values.id}&merchantsId=${values.merchantsId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  detailDataMerchant: (values, cb) => {
    let data = {
      url: `corporate/detail?userId=${values.userId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantReward: (values, cb) => {
    let data = {
      url: `reward/list?merchantId=${values.merchantId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantListItem: (values, cb) => {
    let data = {
      url: `item/list?merchantId=${values.merchantId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantLocation: (values, cb) => {
    let data = {
      url: `member/location?merchantId=${values.merchantId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  merchantListData: (values, cb) => {
    let data = {
      url: `corporate/merchant_api?sort=${values.sort}&city=${values.city}&offset=${values.offset}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  }
}
