import CoreApi from './core'

export default {
  broadcastList: (values, cb) => {
    let data = {
      url: `broadcast/broadcast?memberId=${values.memberId}&offset=${values.offset}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  broadcastDetail: (values, cb) => {
    let data = {
      url: `broadcast/detail?broadcastId=${values.broadcastId}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  readBroadcast: (values, cb) => {
    let data = {
      url: `broadcast/readbroadcast`,
      token: values.token,
      bodyData: {
        broadcastId: values.broadcastId,
        memberId: values.memberId
      }
    }
    CoreApi.POST(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  }
}
