const domainUrl = 'http://www.kartukami.com/api_v3/'
const TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ODd9.G-DgRmrYtxjGyikB4-LWkGvTeEL8-nYtuM3IcpnTxlU'

export default {
  GET: (data) => new Promise((resolve, reject) => {
    const url = domainUrl + data.url
    fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': TOKEN, //data.token,
        'Content-type': 'application/json',
        "Cache-Control": "no-cache"
      },
    }).then((response) => response.json())
    .then((responseText) => {
      resolve(responseText)
    })
    .catch((error) => {
      reject(error)
    });
  }),

  POST: (data) => new Promise((resolve, reject) => {
    const url = domainUrl + data.url

    fetch(url, {
      method: 'POST',
      headers: {
        'Authorization': TOKEN, //data.token,
        'Content-type': 'application/json'
      },
      body: JSON.stringify(data.bodyData),
    }).then((response) => response.json())
    .then((responseText) => {
      resolve(responseText);
    })
    .catch((error) => {
      reject(error);
    });
  })
}
