import CoreApi from './core'

export default {
  checkNumber: (values, cb) => {
    let data = {
      url: `member/checkphone?phone=${values.number}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  createMember: (values, cb) => {
    let data = {
      url: `member/create`,
      token: values.token,
      bodyData : values.bodyValues,
    }
    CoreApi.POST(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  getSingleMember: (values, cb) => {
    let data = {
    url: `member/members?id=${values.id}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  checkEmail: (values, cb) => {
    let data = {
      url: `member/checkemail?email=${values.email}`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },

  updatePlayerId: (values, cb) => {
    let data = {
      url: `member/updateplayerid`,
      token: values.token,
      bodyData: {
        memberId: values.memberId,
        playerId: values.playerId
      }
    }

    CoreApi.POST(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  },
  deletePlayerId: (values, cb) => {
    let data = {
      url: `member/deleteplayerid`,
      token: values.token,
      bodyData: {
        playerId: values.playerId
      }
    }

    CoreApi.POST(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  }
}
