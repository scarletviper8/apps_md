import CoreApi from './core'

export default {
  billboard: (values, cb) => {
    let data = {
      url: `corporate/billboard`,
      token: values.token
    }
    CoreApi.GET(data).then((result) => {
      cb(result)
    }).catch((error) => {
      cb(error)
    })
  }
}
