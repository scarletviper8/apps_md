import React from 'react';
import { StatusBar, View, Platform } from 'react-native';
import {ThemeProvider} from "styled-components/native";
import { Provider } from 'react-redux';

import store from './store'
import AppWithNavigationState from './navigators/AppWithNavigationState';

const theme = {
  // regular: 'Lato-Regular',
  // bold:'Lato-Bold'
};

export default function App() {
  return (
    <Provider store={store}>
      <View style={{flex:1}}>
        <StatusBar
          backgroundColor="rgba(12, 52 , 61, 1)"
          barStyle={ Platform.OS == 'android' ? 'light-content' : 'dark-content' }
          />
        <ThemeProvider theme={theme}>
          <AppWithNavigationState />
        </ThemeProvider>
      </View>
    </Provider>
  )
}
